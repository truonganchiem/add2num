//Ham dao nguoc chuoi
function reverse(str) {
    var temp = [];
    for (var i = str.length - 1; i >= 0; i--) {
        temp.push(str[i]);
    }
    return temp.join('');
}

//Ham cong 2 so
function sum(stn1, stn2) {
    stn1 = reverse(stn1);
    stn2 = reverse(stn2);
    var max = stn1.length > stn2.length ? stn1.length : stn2.length;
    var result = [];
    var remember = 0;
    for (var i = 0; i < max; i++) {
        var tempRemember = void 0;
        var tempNum = void 0;
        if (stn1[i] && !stn2[i]) {
            tempNum = parseInt(stn1[i]);
            console.log(`Còn ${tempNum} nên cộng với nhớ ${remember} bằng  ${tempNum + remember}`);
        }
        else if (!stn1[i] && stn2[i]) {
            tempNum = parseInt(stn2[i]);
            console.log(`Còn ${tempNum} nên cộng với nhớ ${remember} bằng  ${tempNum + remember}`);
        }
        else {
            tempNum = parseInt(stn1[i]) + parseInt(stn2[i]);
            if (remember !== 0) {
                console.log(`Lấy ${stn1[i]} + ${stn2[i]} bằng ${tempNum} . Cộng tiếp với nhớ ${remember} được ${tempNum+remember}`);
            }
            else {
                console.log(`Lấy ${stn1[i]} + ${stn2[i]} bằng ${tempNum}`);
            }
        }
        var num2 = void 0;
        if (tempNum >= 10) {
            num2 = tempNum % 10;
            tempRemember = Math.floor(tempNum / 10);
        }
        else {
            num2 = tempNum;
            tempRemember = 0;
        }
        num2 = num2 + remember;

        console.log(`Lưu ${num2} vào kết quả và nhớ ${tempRemember}`);
        result.push(String(num2));

        remember = tempRemember;
    }
    if (remember !== 0)
        result.push(String(remember));
    console.log(`Kết quả phép cộng ${reverse(stn1)} và ${reverse(stn2)} bằng ${reverse(result.join(''))}`);

    return reverse(result.join(''))
}

//test hàm bằng cách thay đổi 2 tham số của hàm sum.
//Lưu ý : 2 tham số phải dạng string .
sum("1234", "897");


module.exports = sum