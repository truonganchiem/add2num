# Hướng dẫn chạy
Để chạy được chương trình đầu tiên phải đảm bảo đã cài đặt NodeJS để chạy được Javascript.

# Run File Add2Num.js
- B1 : Clone source về với câu lệnh : git clone https://gitlab.com/truonganchiem/add2num.git

- B2 : Mở CMD ở thư mục vừa tải về chạy lệnh "Node Add2Num.js" để chạy file với tham số đầu vào cho sẳn là "1234" và "897" kết quà là "2131".

(có thể thay đổi 2 tham số đầu vào của hàm sum trong file Add2Num.js ở dòng 62 để test được với 2 tham số khác tùy ý)


(!Lưu ý : 2 tham số phải dạng chuỗi (String))

# Unit Test
Để chạy UnitTesting mà em đã viết sẳn 10 test case cần đến thư mục UnitTesting.

- B1 : Chạy lệnh "npm i" để cài node_modules thư viện Jest để thực hiên test.

- B2 : Nhập lệnh "npm run test" để chạy các test case đã viết sẳn ở file test.js
