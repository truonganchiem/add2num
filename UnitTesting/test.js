const sum = require('../Add2Num.js')

describe("sum suite", function() {
    test("test 1", function() {
      expect(sum("1234", "897")).toBe("2131");
    });
    test("test 2", function() {
      expect(sum("274", "1835")).toBe("2109");
    });
    test("test 3", function() {
      expect(sum("860", "333")).toBe("1193");
    });
    test("test 4", function() {
      expect(sum("8", "15")).toBe("23");
    });
    test("test 5", function() {
      expect(sum("7", "862")).toBe("869");
    });
    test("test 6", function() {
      expect(sum("135", "302")).toBe("437");
    });
    test("test 7", function() {
      expect(sum("208", "703")).toBe("911");
    });
    test("test 8", function() {
      expect(sum("98532", "756301")).toBe("854833");
    });
    test("test 9", function() {
      expect(sum("87536", "684")).toBe("88220");
    });
    test("test 10", function() {
      expect(sum("11475", "32971")).toBe("44446");
    });
   
  });